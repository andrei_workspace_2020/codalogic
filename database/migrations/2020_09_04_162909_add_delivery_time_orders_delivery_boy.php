<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryTimeOrdersDeliveryBoy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_to_delivery_boy', function (Blueprint $table) {
            //
            $table->integer('delivery_time')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_to_delivery_boy', function (Blueprint $table) {
            //
            $table->dropColumn('delivery_time');
        });
    }
}
