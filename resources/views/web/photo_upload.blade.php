@extends('web.layout')

@section('content')
<!-- login Content -->
<div class="container-fuild">
	<nav aria-label="breadcrumb">
		<div class="container">
			<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">@lang('website.Home')</a></li>
			</ol>
		</div>
	  </nav>
  </div> 

<section class="page-area pro-content">
	<div class="container"> 
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-md-6">
				<div class="col-12">
					<h4 class="heading login-heading text-center">@lang("website.We'll need to see your ID to ensure")</h4>
					<p class="text-center">@lang("website.Please upload a selfie of you holding your valid ID.")</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-md-6 justify-content-center">
				@if(Session::has('loginError'))
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="">@lang('website.Error'):</span>
						{!! session('loginError') !!}

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="">@lang('website.success'):</span>
						{!! session('success') !!}

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif

				@if( count($errors) > 0)
					@foreach($errors->all() as $error)
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">@lang('website.Error'):</span>
						{{ $error }}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					@endforeach
				@endif

				@if(Session::has('error'))
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">@lang('website.Error'):</span>
						{!! session('error') !!}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">@lang('website.Success'):</span>
						{!! session('success') !!}

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
			</div>
		</div>
		<div class="row justify-content-center">	
			<div class="col-12 col-sm-12 col-md-6">
				<div class="col-12 my-5 px-0" id="registerTabContent">
					<div class="registration-process">
						<form name="phone_verify" enctype="multipart/form-data"  action="{{ URL::to('/photoUploadProcess')}}" method="post">
							{{csrf_field()}}
							<div class="col-12 col-sm-12 mb-5 text-center">
								<img id="image_id_card" src="{{asset('/images/ID_selfie.png')}}">
							</div>
							<div class="col-12 col-sm-12 my-2 text-center">
								<input type="file" name="file_id_card" id="file_id_card" style="display: none;">
								<button type="button" id="btn_upload_id_card" class="btn btn-light swipe-to-top">
									@lang('website.Upload Selfie With Your ID')
								</button>
							</div>
							<div class="col-12 col-sm-12 my-2 text-center">
								<button type="submit" class="btn btn-light swipe-to-top">@lang('website.Submit') </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection