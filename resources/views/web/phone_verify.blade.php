@extends('web.layout')

@section('content')
<!-- login Content -->
<div class="container-fuild">
	<nav aria-label="breadcrumb">
		<div class="container">
			<ol class="breadcrumb">
			  <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">@lang('website.Home')</a></li>
			</ol>
		</div>
	  </nav>
  </div> 

<section class="page-area pro-content">
	<div class="container"> 
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-md-6">
				<div class="col-12">
					<h4 class="heading login-heading text-center">@lang('website.Can we get your phone number?')</h4>
					<p>@lang("website.Enter your mobile number and we'll send a verification code to your phone.")</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-md-6 justify-content-center">
				@if(Session::has('loginError'))
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="">@lang('website.Error'):</span>
						{!! session('loginError') !!}

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="">@lang('website.success'):</span>
						{!! session('success') !!}

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif

				@if( count($errors) > 0)
					@foreach($errors->all() as $error)
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">@lang('website.Error'):</span>
						{{ $error }}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					@endforeach
				@endif

				@if(Session::has('error'))
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">@lang('website.Error'):</span>
						{!! session('error') !!}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">@lang('website.Success'):</span>
						{!! session('success') !!}

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
			</div>
		</div>
		<div class="row justify-content-center">	
			<div class="col-12 col-sm-12 col-md-6">
				<div class="col-12 my-5 px-0" id="registerTabContent">
					<div class="registration-process">
						<form name="phone_verify" enctype="multipart/form-data"  action="{{ URL::to('/phoneVerifyProcess')}}" method="post">
							{{csrf_field()}}

							<div class="from-group mb-3">
								<div class="col-12"> <label for="inlineFormInputGroup"><strong style="color: red;">*</strong>@lang('website.Phone Number')</label></div>
								<div class="input-group col-12">
									<input  name="phone" type="text" class="form-control field-validate" id="phone" placeholder="@lang('website.Please enter your phone number')" value="{{ old('phone') }}">
									<span class="help-block" hidden>@lang('website.Please enter your phone number')</span>
								</div>
							</div>
							<div class="col-12 col-sm-12">
								<button type="submit" class="btn btn-light swipe-to-top">@lang('website.Send Verification Code') </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection