@extends('admin.layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.EditCustomers') }} <small>{{ trans('labels.EditCurrentCustomers') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li><a href="{{ URL::to('admin/customers/display')}}"><i class="fa fa-users"></i> {{ trans('labels.ListingAllCustomers') }}</a></li>
            <li class="active">{{ trans('labels.EditCustomers') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('labels.EditCustomers') }} </h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-info">
                                    <!--<div class="box-header with-border">
                                          <h3 class="box-title">Edit category</h3>
                                        </div>-->
                                    <!-- /.box-header -->
                                    <br>
                                    @if (count($errors) > 0)
                                      @if($errors->any())
                                      <div class="alert alert-danger alert-dismissible" role="alert">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          {{$errors->first()}}
                                      </div>
                                      @endif
                                    @endif


                                    <!-- form start -->
                                    <div class="box-body">

                                        {!! Form::open(array('url' =>'admin/customers/update', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}

                                        {!! Form::hidden('customers_id', $data['customers']->id, array('class'=>'form-control', 'id'=>'id')) !!}

                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.FirstName') }}* </label>
                                            <div class="col-sm-10 col-md-4">
                                                {!! Form::text('first_name', $data['customers']->first_name, array('class'=>'form-control field-validate', 'id'=>'first_name')) !!}
                                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.FirstNameText') }}</span>
                                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.LastName') }}*</label>
                                            <div class="col-sm-10 col-md-4">
                                                {!! Form::text('last_name', $data['customers']->last_name , array('class'=>'form-control field-validate', 'id'=>'last_name')) !!}
                                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.lastNameText') }}</span>
                                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Gender') }}*</label>
                                            <div class="col-sm-10 col-md-4">
                                                <label>
                                                    <input @if($data['customers']->gender == 0 or empty($data['customers']->gender)) checked @endif type="radio" name="gender" value="0" class="minimal">
                                                    {{ trans('labels.Male') }}
                                                </label><br>

                                                <label>
                                                    <input @if($data['customers']->gender == 1 and !empty($data['customers']->gender)) checked @endif type="radio" name="gender" value="1" class="minimal">
                                                    {{ trans('labels.Female') }}
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.DOB') }}*</label>
                                            <div class="col-sm-10 col-md-4">
                                                {!! Form::text('dob', $data['customers']->dob, array('class'=>'form-control' , 'id'=>'dob')) !!}
                                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.customers_dobText') }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                          <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Telephone') }}</label>
                                          <div class="col-sm-10 col-md-4">
                                            {!! Form::text('phone',  $data['customers']->phone, array('class'=>'form-control phone-validate', 'id'=>'phone')) !!}
                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.TelephoneText') }}</span>
                                          </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.changePassword') }}</label>
                                            <div class="col-sm-10 col-md-4">
                                                {!! Form::checkbox('changePassword', 'yes', null, ['class' => '', 'id'=>'change-passowrd']) !!}
                                            </div>
                                        </div>

                                        <!-- <div class="form-group password_content">-->
                                        <div class="form-group password" style="display: none">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Password') }}*</label>
                                            <div class="col-sm-10 col-md-4">
                                                {!! Form::password('password', array('class'=>'form-control ', 'id'=>'password')) !!}
                                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                    {{ trans('labels.PasswordText') }}</span>
                                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Status') }}
                                            </label>
                                            <div class="col-sm-10 col-md-4">
                                                <select class="form-control" name="status">
                                                    <option @if($data['customers']->status == 1)
                                                        selected
                                                        @endif
                                                        value="1">{{ trans('labels.Active') }}</option>
                                                    <option @if($data['customers']->status == 0)
                                                        selected
                                                        @endif
                                                        value="0">{{ trans('labels.Inactive') }}</option>
                                                </select><span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.StatusText') }}</span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Approved?') }}
                                            </label>
                                            <div class="col-sm-10 col-md-4">
                                                <select class="form-control" name="approved">
                                                    <option @if($data['customers']->approved == 1)
                                                        selected
                                                        @endif
                                                        value="1">{{ trans('labels.Yes') }}</option>
                                                    <option @if($data['customers']->approved == 0)
                                                        selected
                                                        @endif
                                                        value="0">{{ trans('labels.No') }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.ID Photo') }}
                                            </label>
                                            <div class="col-sm-10 col-md-4">
                                                <div class="row">
                                                    <div class="col-sm-9 text-center">
                                                        <input type="hidden" name="old_id_photo" value="{{$data['customers']->id_photo ? $data['customers']->id_photo : ''}}">
                                                        <img id="image_id_card" src="{{$data['customers']->id_photo && file_exists(public_path().'/images/customers_id/'.$data['customers']->id_photo) ? asset('/images/customers_id/'.$data['customers']->id_photo) : asset('/images/ID_selfie.png')}}" style="width: 100%">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="file" name="file_id_card" id="file_id_card" style="display: none;">
                                                        <button type="button" id="btn_upload_id_card" class="btn btn-light swipe-to-top">@lang('website.Update ID')</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- /.box-body -->
                                        <div class="box-footer text-center">
                                            <button type="submit" class="btn btn-primary">{{ trans('labels.Submit') }} </button>
                                            <a href="{{ URL::to('admin/customers/approved/display')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
                                        </div>
                                        <!-- /.box-footer -->
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('labels.Order History') }} </h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-info">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>{{ trans('labels.ID') }}</th>
                                                        <th>{{ trans('labels.CustomerName') }}</th>
                                                        <th>{{ trans('labels.Order Source') }}</th>
                                                        <th>{{ trans('labels.OrderTotal') }}</th>
                                                        <th>{{ trans('labels.DatePurchased') }}</th>
                                                        <th>{{ trans('labels.DeliveryTime') }}</th>
                                                        <th>{{ trans('labels.Status') }} </th>
                                                        <th>{{ trans('labels.deliveryboy status') }} </th>
                                                        <th>{{ trans('labels.Action') }}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($listingOrders['orders'])>0)
                                                        @foreach ($listingOrders['orders'] as $key=>$orderData)
                                                            <tr>
                                                                <td>{{ $orderData->orders_id }}</td>
                                                                <td>{{ $orderData->customers_name }}</td>
                                                                <td>
                                                                    @if($orderData->ordered_source == 1)
                                                                    {{ trans('labels.Website') }}
                                                                    @else
                                                                    {{ trans('labels.Application') }}
                                                                    @endif</td>
                                                                <td>
                                                                    
                                                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $orderData->order_price }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif</td>
                                                                <td>{{ date('d/m/Y', strtotime($orderData->date_purchased)) }}</td>
                                                                <td>
                                                                    @if($orderData->delivery_from!=-1 && $orderData->delivery_to!=-1)
                                                                    <span class="{{$orderData->badge_color ? 'label label-'.$orderData->badge_color : ''}}">
                                                                        {{date("g:i A", strtotime($orderData->delivery_from.":00")).' - '. date("g:i A", strtotime($orderData->delivery_to.":00"))}}
                                                                    </span>
                                                                    @else <span>---</span>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($orderData->orders_status_id==1)
                                                                        <span class="label label-warning">
                                                                    @elseif($orderData->orders_status_id==2)
                                                                        <span class="label label-success">
                                                                    @elseif($orderData->orders_status_id==3)
                                                                        <span class="label label-danger">
                                                                    @else
                                                                        <span class="label label-primary">
                                                                    @endif
                                                                    {{ $orderData->orders_status }}
                                                                        </span>
                                                                </td>
                                                                <td>
                                                                    @if($orderData->deliveryboy_orders_status != '')
                                                                        <span class="label label-primary">
                                                                            {{ $orderData->deliveryboy_orders_status }}
                                                                        </span>
                                                                    @else
                                                                    ---
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <a data-toggle="tooltip" data-placement="bottom" title="View Order" href="/admin/orders/vieworder/{{ $orderData->orders_id }}" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                                                    <a data-toggle="tooltip" data-placement="bottom" title="Delete Order" id="deleteOrdersId" orders_id ="{{ $orderData->orders_id }}" class="badge bg-red"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                                <div class="col-xs-12 text-right">
                                                    {{$listingOrders['orders']->links()}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- deleteModal -->
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="deleteModalLabel">{{ trans('labels.DeleteOrder') }}</h4>
                        </div>
                        {!! Form::open(array('url' =>'admin/orders/deleteOrder', 'name'=>'deleteOrder', 'id'=>'deleteOrder', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                        {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                        {!! Form::hidden('orders_id',  '', array('class'=>'form-control', 'id'=>'orders_id')) !!}
                        <div class="modal-body">
                            <p>{{ trans('labels.DeleteOrderText') }}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('labels.Close') }}</button>
                            <button type="submit" class="btn btn-primary" id="deleteOrder">{{ trans('labels.Delete') }}</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        <!-- Main row -->

        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
