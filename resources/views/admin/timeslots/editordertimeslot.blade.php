@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> {{ trans('labels.EditOrderTimeSlot') }} <small>{{ trans('labels.EditOrderTimeSlot') }}...</small> </h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
                <li><a href="{{ URL::to('admin/orderstimeslots')}}"><i class="fa fa-dashboard"></i>{{ trans('labels.ListingOrderTimeSlots') }}</a>
                <li class="active">{{ trans('labels.EditOrderTimeSlot') }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->

            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{ trans('labels.EditOrderTimeSlot') }}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    @if (count($errors) > 0)
                                        @if($errors->any())
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <!-- form start -->
                                        <div class="box-body">

                                            {!! Form::open(array('url' =>'admin/updateOrderTimeSlot', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                                              {!! Form::hidden('orders_timeslot_id', $result['order_timeslot']->orders_timeslot_id) !!}
                                              

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.From') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <select name="from" class="form-control">
                                                        <option>{{ trans('labels.Select Time Slot')}}</option>
                                                        <option value="0" @if($result['order_timeslot']->from==0) selected @endif>12:00 AM</option>
                                                        <option value="1" @if($result['order_timeslot']->from==1) selected @endif>1:00 AM</option>
                                                        <option value="2" @if($result['order_timeslot']->from==2) selected @endif>2:00 AM</option>
                                                        <option value="3" @if($result['order_timeslot']->from==3) selected @endif>3:00 AM</option>
                                                        <option value="4" @if($result['order_timeslot']->from==4) selected @endif>4:00 AM</option>
                                                        <option value="5" @if($result['order_timeslot']->from==5) selected @endif>5:00 AM</option>
                                                        <option value="6" @if($result['order_timeslot']->from==6) selected @endif>6:00 AM</option>
                                                        <option value="7" @if($result['order_timeslot']->from==7) selected @endif>7:00 AM</option>
                                                        <option value="8" @if($result['order_timeslot']->from==8) selected @endif>8:00 AM</option>
                                                        <option value="9" @if($result['order_timeslot']->from==9) selected @endif>9:00 AM</option>
                                                        <option value="10" @if($result['order_timeslot']->from==10) selected @endif>10:00 AM</option>
                                                        <option value="11" @if($result['order_timeslot']->from==11) selected @endif>11:00 AM</option>
                                                        <option value="12" @if($result['order_timeslot']->from==12) selected @endif>12:00 PM</option>
                                                        <option value="13" @if($result['order_timeslot']->from==13) selected @endif>1:00 PM</option>
                                                        <option value="14" @if($result['order_timeslot']->from==14) selected @endif>2:00 PM</option>
                                                        <option value="15" @if($result['order_timeslot']->from==15) selected @endif>3:00 PM</option>
                                                        <option value="16" @if($result['order_timeslot']->from==16) selected @endif>4:00 PM</option>
                                                        <option value="17" @if($result['order_timeslot']->from==17) selected @endif>5:00 PM</option>
                                                        <option value="18" @if($result['order_timeslot']->from==18) selected @endif>6:00 PM</option>
                                                        <option value="19" @if($result['order_timeslot']->from==19) selected @endif>7:00 PM</option>
                                                        <option value="20" @if($result['order_timeslot']->from==20) selected @endif>8:00 PM</option>
                                                        <option value="21" @if($result['order_timeslot']->from==21) selected @endif>9:00 PM</option>
                                                        <option value="22" @if($result['order_timeslot']->from==22) selected @endif>10:00 PM</option>
                                                        <option value="23" @if($result['order_timeslot']->from==23) selected @endif>11:00 PM</option>
                                                    </select>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                      {{ trans('labels.Select Time') }}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.To') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <select name="to" class="form-control">
                                                        <option>{{ trans('labels.Select Time Slot')}}</option>
                                                        <option value="0" @if($result['order_timeslot']->to==0) selected @endif>12:00 AM</option>
                                                        <option value="1" @if($result['order_timeslot']->to==1) selected @endif>1:00 AM</option>
                                                        <option value="2" @if($result['order_timeslot']->to==2) selected @endif>2:00 AM</option>
                                                        <option value="3" @if($result['order_timeslot']->to==3) selected @endif>3:00 AM</option>
                                                        <option value="4" @if($result['order_timeslot']->to==4) selected @endif>4:00 AM</option>
                                                        <option value="5" @if($result['order_timeslot']->to==5) selected @endif>5:00 AM</option>
                                                        <option value="6" @if($result['order_timeslot']->to==6) selected @endif>6:00 AM</option>
                                                        <option value="7" @if($result['order_timeslot']->to==7) selected @endif>7:00 AM</option>
                                                        <option value="8" @if($result['order_timeslot']->to==8) selected @endif>8:00 AM</option>
                                                        <option value="9" @if($result['order_timeslot']->to==9) selected @endif>9:00 AM</option>
                                                        <option value="10" @if($result['order_timeslot']->to==10) selected @endif>10:00 AM</option>
                                                        <option value="11" @if($result['order_timeslot']->to==11) selected @endif>11:00 AM</option>
                                                        <option value="12" @if($result['order_timeslot']->to==12) selected @endif>12:00 PM</option>
                                                        <option value="13" @if($result['order_timeslot']->to==13) selected @endif>1:00 PM</option>
                                                        <option value="14" @if($result['order_timeslot']->to==14) selected @endif>2:00 PM</option>
                                                        <option value="15" @if($result['order_timeslot']->to==15) selected @endif>3:00 PM</option>
                                                        <option value="16" @if($result['order_timeslot']->to==16) selected @endif>4:00 PM</option>
                                                        <option value="17" @if($result['order_timeslot']->to==17) selected @endif>5:00 PM</option>
                                                        <option value="18" @if($result['order_timeslot']->to==18) selected @endif>6:00 PM</option>
                                                        <option value="19" @if($result['order_timeslot']->to==19) selected @endif>7:00 PM</option>
                                                        <option value="20" @if($result['order_timeslot']->to==20) selected @endif>8:00 PM</option>
                                                        <option value="21" @if($result['order_timeslot']->to==21) selected @endif>9:00 PM</option>
                                                        <option value="22" @if($result['order_timeslot']->to==22) selected @endif>10:00 PM</option>
                                                        <option value="23" @if($result['order_timeslot']->to==23) selected @endif>11:00 PM</option>
                                                    </select>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                      {{ trans('labels.Select Time') }}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Badge Color') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <p>
                                                        <input type="radio" name="badge_color" value="primary" @if($result['order_timeslot']->badge_color=="primary") checked @endif>
                                                        <span class="label label-primary">Primary</span>
                                                    </p>
                                                    <p>
                                                        <input type="radio" name="badge_color" value="info" @if($result['order_timeslot']->badge_color=="info") checked @endif>
                                                        <span class="label label-info">info</span>
                                                    </p>
                                                    <p>
                                                        <input type="radio" name="badge_color" value="success" @if($result['order_timeslot']->badge_color=="success") checked @endif>
                                                        <span class="label label-success">Success</span>
                                                    </p>
                                                    <p>
                                                        <input type="radio" name="badge_color" value="warning" @if($result['order_timeslot']->badge_color=="warning") checked @endif>
                                                        <span class="label label-warning">Warning</span>
                                                    </p>
                                                    <p>
                                                        <input type="radio" name="badge_color" value="danger" @if($result['order_timeslot']->badge_color=="danger") checked @endif>
                                                        <span class="label label-danger">Danger</span>
                                                    </p>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                      {{ trans('labels.Select Color') }}</span>
                                                </div>
                                            </div>

                                            <!-- /.box-body -->
                                            <div class="box-footer text-right">
                                                <div class="col-sm-offset-2 col-md-offset-3 col-sm-10 col-md-4">
                                                    <button type="submit" class="btn btn-primary">{{ trans('labels.Submit') }}</button>
                                                    <a href="../ordertimeslots" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
                                                </div>
                                            </div>
                                            <!-- /.box-footer -->
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Main row -->

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
