@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> {{ trans('labels.AddOrderTimeSlot') }} <small>{{ trans('labels.AddOrderTimeSlot') }}...</small> </h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
                <li><a href="{{ URL::to('admin/ordertimeslots')}}"><i class="fa fa-dashboard"></i>{{ trans('labels.ListingOrderTimeSlots') }}</a></li>
                <li class="active">{{ trans('labels.AddOrderTimeSlot') }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->

            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{ trans('labels.AddOrderTimeSlot') }}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    @if (count($errors) > 0)
                                        @if($errors->any())
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <!-- form start -->
                                        <div class="box-body">

                                            {!! Form::open(array('url' =>'admin/addNewOrderTimeSlot', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.From') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <select name="from" class="form-control field-validate">
                                                        <option value="0">12:00 AM</option>
                                                        <option value="1">1:00 AM</option>
                                                        <option value="2">2:00 AM</option>
                                                        <option value="3">3:00 AM</option>
                                                        <option value="4">4:00 AM</option>
                                                        <option value="5">5:00 AM</option>
                                                        <option value="6">6:00 AM</option>
                                                        <option value="7">7:00 AM</option>
                                                        <option value="8">8:00 AM</option>
                                                        <option value="9">9:00 AM</option>
                                                        <option value="10">10:00 AM</option>
                                                        <option value="11">11:00 AM</option>
                                                        <option value="12">12:00 PM</option>
                                                        <option value="13">1:00 PM</option>
                                                        <option value="14">2:00 PM</option>
                                                        <option value="15">3:00 PM</option>
                                                        <option value="16">4:00 PM</option>
                                                        <option value="17">5:00 PM</option>
                                                        <option value="18">6:00 PM</option>
                                                        <option value="19">7:00 PM</option>
                                                        <option value="20">8:00 PM</option>
                                                        <option value="21">9:00 PM</option>
                                                        <option value="22">10:00 PM</option>
                                                        <option value="23">11:00 PM</option>
                                                    </select>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                      {{ trans('labels.Select Time') }}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.To') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <select name="to" class="form-control field-validate">
                                                        <option value="0">12:00 AM</option>
                                                        <option value="1">1:00 AM</option>
                                                        <option value="2">2:00 AM</option>
                                                        <option value="3">3:00 AM</option>
                                                        <option value="4">4:00 AM</option>
                                                        <option value="5">5:00 AM</option>
                                                        <option value="6">6:00 AM</option>
                                                        <option value="7">7:00 AM</option>
                                                        <option value="8">8:00 AM</option>
                                                        <option value="9">9:00 AM</option>
                                                        <option value="10">10:00 AM</option>
                                                        <option value="11">11:00 AM</option>
                                                        <option value="12">12:00 PM</option>
                                                        <option value="13">1:00 PM</option>
                                                        <option value="14">2:00 PM</option>
                                                        <option value="15">3:00 PM</option>
                                                        <option value="16">4:00 PM</option>
                                                        <option value="17">5:00 PM</option>
                                                        <option value="18">6:00 PM</option>
                                                        <option value="19">7:00 PM</option>
                                                        <option value="20">8:00 PM</option>
                                                        <option value="21">9:00 PM</option>
                                                        <option value="22">10:00 PM</option>
                                                        <option value="23">11:00 PM</option>
                                                    </select>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                      {{ trans('labels.Select Time') }}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Badge Color') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <p>
                                                        <input type="radio" name="badge_color" value="primary" checked>
                                                        <span class="label label-primary">Primary</span>
                                                    </p>
                                                    <p>
                                                        <input type="radio" name="badge_color" value="info">
                                                        <span class="label label-info">info</span>
                                                    </p>
                                                    <p>
                                                        <input type="radio" name="badge_color" value="success">
                                                        <span class="label label-success">Success</span>
                                                    </p>
                                                    <p>
                                                        <input type="radio" name="badge_color" value="warning">
                                                        <span class="label label-warning">Warning</span>
                                                    </p>
                                                    <p>
                                                        <input type="radio" name="badge_color" value="danger">
                                                        <span class="label label-danger">Danger</span>
                                                    </p>
                                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                      {{ trans('labels.Select Color') }}</span>
                                                </div>
                                            </div>

                                            <!-- /.box-body -->
                                            <div class="box-footer text-right">
                                                <div class="col-sm-offset-2 col-md-offset-3 col-sm-10 col-md-4">
                                                    <button type="submit" class="btn btn-primary">{{ trans('labels.Submit') }}</button>
                                                    <a href="ordertimeslots" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
                                                </div>
                                            </div>
                                            <!-- /.box-footer -->
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Main row -->

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
