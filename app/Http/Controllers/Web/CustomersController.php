<?php
namespace App\Http\Controllers\Web;

use App\Http\Controllers\Web\AlertController;
use App\Models\Web\Cart;
use App\Models\Web\Currency;
use App\Models\Web\Customer;
use App\Models\Web\Index;
use App\Models\Web\Languages;
use App\Models\Web\Products;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Lang;
use Session;
use Socialite;
use Validator;
use Hash;
use Twilio\Rest\Client;

class CustomersController extends Controller
{

    public function __construct(
        Index $index,
        Languages $languages,
        Products $products,
        Currency $currency,
        Customer $customer,
        Cart $cart
    ) {
        $this->index = $index;
        $this->languages = $languages;
        $this->products = $products;
        $this->currencies = $currency;
        $this->customer = $customer;
        $this->cart = $cart;
        $this->theme = new ThemeController();
    }

    public function signup(Request $request)
    {
        $final_theme = $this->theme->theme();
        if (auth()->guard('customer')->check()) {
            return redirect('/');
        } else {
            $title = array('pageTitle' => Lang::get("website.Sign Up"));
            $result = array();
            $result['commonContent'] = $this->index->commonContent();
            return view("login", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
        }
    }

    public function login(Request $request)
    {
        $result = array();
        if (auth()->guard('customer')->check()) {
            return redirect('/');
        } else {
            $result['cart'] = $this->cart->myCart($result);

            if (count($result['cart']) != 0) {
                $result['checkout_button'] = 1;
            } else {
                $result['checkout_button'] = 0;

            }
            $previous_url = Session::get('_previous.url');

            $ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
            $ref = rtrim($ref, '/');

            session(['previous' => $previous_url]);

            $title = array('pageTitle' => Lang::get("website.Login"));
            $final_theme = $this->theme->theme();

            $result['commonContent'] = $this->index->commonContent();
            return view("auth.login", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
        }

    }

    public function processLogin(Request $request)
    {
        $old_session = Session::getId();

        $result = array();

        //check authentication of email and password
        $customerInfo = array("email" => $request->email, "password" => $request->password);

        if (auth()->guard('customer')->attempt($customerInfo)) {
            $customer = auth()->guard('customer')->user();
            if ($customer->role_id != 2) {
                $record = DB::table('settings')->where('id', 94)->first();
                if ($record->value == 'Maintenance' && $customer->role_id == 1) {
                    auth()->attempt($customerInfo);
                } else {
                    Auth::guard('customer')->logout();
                    return redirect('login')->with('loginError', Lang::get("website.You Are Not Allowed With These Credentials!"));
                }
            }
            if($customer->status==0){
                Auth::guard('customer')->logout();
                return redirect('login')->with('loginError', Lang::get("website.You Are Not Allowed To Login!"));
            }
            $result = $this->customer->processLogin($request, $old_session);
            if (!empty(session('previous'))) {
                return Redirect::to(session('previous'));
            } else {
                
                Session::forget('guest_checkout');
                return redirect()->intended('/')->with('result', $result);
            }
        } else {
            return redirect('login')->with('loginError', Lang::get("website.Email or password is incorrect"));
        }
        //}
    }

    public function addToCompare(Request $request)
    {
        $cartResponse = $this->customer->addToCompare($request);
        return $cartResponse;
    }

    public function DeleteCompare($id)
    {
        $Response = $this->customer->DeleteCompare($id);
        return redirect()->back()->with($Response);
    }

    public function Compare()
    {
        $result = array();
        $final_theme = $this->theme->theme();
        $result['commonContent'] = $this->index->commonContent();
        $compare = $this->customer->Compare();
        $results = array();
        foreach ($compare as $com) {
            $data = array('products_id' => $com->product_ids, 'page_number' => '0', 'type' => 'compare', 'limit' => '15', 'min_price' => '', 'max_price' => '');
            $newest_products = $this->products->products($data);
            array_push($results, $newest_products);
        }
        $result['products'] = $results;
        return view('web.compare', ['result' => $result, 'final_theme' => $final_theme]);
    }

    public function profile()
    {
        $title = array('pageTitle' => Lang::get("website.Profile"));
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();
        return view('web.profile', ['result' => $result, 'title' => $title, 'final_theme' => $final_theme]);
    }

    public function updateMyProfile(Request $request)
    {
        $validated = $request->validate([
            'customers_firstname' => 'required',
            'customers_lastname' => 'required',
            'gender' => 'required',
            'customers_telephone'=> 'required | numeric | unique:users',
            'file_id_card' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        if ($request->hasFile('file_id_card')) {
            //  Let's do everything here
            if ($request->file('file_id_card')->isValid()) {
                $message = $this->customer->updateMyProfile($request);
                return redirect()->back()->with('success', $message);
            }
        }
    }

    public function changePassword()
    {
        $title = array('pageTitle' => Lang::get("website.Change Password"));
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();
        return view('web.changepassword', ['result' => $result, 'title' => $title, 'final_theme' => $final_theme]);
    }

    public function updateMyPassword(Request $request)
    {
        $password = Auth::guard('customer')->user()->password;
        if (Hash::check($request->current_password, $password)) {
            $message = $this->customer->updateMyPassword($request);
            return redirect()->back()->with('success', $message);
        }else{
            return redirect()->back()->with('error', lang::get("website.Current password is invalid"));
        }
    }

    public function logout(REQUEST $request)
    {
        Auth::guard('customer')->logout();
        session()->flush();
        $request->session()->forget('customers_id');
        $request->session()->regenerate();
        return redirect()->intended('/');
    }

    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }

    public function handleSocialLoginCallback($social)
    {
        $result = $this->customer->handleSocialLoginCallback($social);
        if (!empty($result)) {
            return redirect()->intended('/')->with('result', $result);
        }
    }

    public function createRandomPassword()
    {
        $pass = substr(md5(uniqid(mt_rand(), true)), 0, 8);
        return $pass;
    }

    public function likeMyProduct(Request $request)
    {
        $cartResponse = $this->customer->likeMyProduct($request);
        return $cartResponse;
    }

    public function unlikeMyProduct(Request $request, $id)
    {

        if (!empty(auth()->guard('customer')->user()->id)) {
            $this->customer->unlikeMyProduct($id);
            $message = Lang::get("website.Product is unliked");
            return redirect()->back()->with('success', $message);
        } else {
            return redirect('login')->with('loginError', 'Please login to like product!');
        }

    }

    public function wishlist(Request $request)
    {
        $title = array('pageTitle' => Lang::get("website.Wishlist"));
        $final_theme = $this->theme->theme();
        $result = $this->customer->wishlist($request);
        return view("web.wishlist", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
    }

    public function loadMoreWishlist(Request $request)
    {

        $limit = $request->limit;

        $data = array('page_number' => $request->page_number, 'type' => 'wishlist', 'limit' => $limit, 'categories_id' => '', 'search' => '', 'min_price' => '', 'max_price' => '');
        $products = $this->products->products($data);
        $result['products'] = $products;

        $cart = '';
        $myVar = new CartController();
        $result['cartArray'] = $this->products->cartIdArray($cart);
        $result['limit'] = $limit;
        return view("web.wishlistproducts")->with('result', $result);

    }

    public function forgotPassword()
    {
        if (auth()->guard('customer')->check()) {
            return redirect('/');
        } else {

            $title = array('pageTitle' => Lang::get("website.Forgot Password"));
            $final_theme = $this->theme->theme();
            $result = array();
            $result['commonContent'] = $this->index->commonContent();
            return view("web.forgotpassword", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
        }
    }

    public function processPassword(Request $request)
    {
        $title = array('pageTitle' => Lang::get("website.Forgot Password"));

        $password = $this->createRandomPassword();

        $email = $request->email;
        $postData = array();

        //check email exist
        $existUser = $this->customer->ExistUser($email);
        if (count($existUser) > 0) {
            $this->customer->UpdateExistUser($email, $password);
            $existUser[0]->password = $password;

            $myVar = new AlertController();
            $alertSetting = $myVar->forgotPasswordAlert($existUser);

            return redirect('login')->with('success', Lang::get("website.Password has been sent to your email address"));
        } else {
            return redirect('forgotPassword')->with('error', Lang::get("website.Email address does not exist"));
        }

    }

    public function recoverPassword()
    {
        $title = array('pageTitle' => Lang::get("website.Forgot Password"));
        $final_theme = $this->theme->theme();
        return view("web.recoverPassword", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
    }

    public function subscribeNotification(Request $request)
    {

        $setting = $this->index->commonContent();

        /* Desktop */
        $type = 3;

        session(['device_id' => $request->device_id]);

        if (!empty(auth()->guard('customers')->user()->id)) {

            $device_data = array(
                'device_id' => $request->device_id,
                'device_type' => $type,
                'register_date' => time(),
                'update_date' => time(),
                'ram' => '',
                'status' => '1',
                'processor' => '',
                'device_os' => '',
                'location' => '',
                'device_model' => '',
                'customers_id' => auth()->guard('customers')->user()->id,
                'manufacturer' => '',
                $setting['setting'][54]->value => '1',
            );

        } else {

            $device_data = array(
                'device_id' => $request->device_id,
                'device_type' => $type,
                'register_date' => time(),
                'update_date' => time(),
                'ram' => '',
                'status' => '1',
                'processor' => '',
                'device_os' => '',
                'location' => '',
                'device_model' => '',
                'manufacturer' => '',
                $setting['setting'][54]->value => '1',
            );

        }
        $this->customer->updateDevice($request, $device_data);
        print 'success';
    }

    public function signupProcess(Request $request){
        $validator = Validator::make(
            array(
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'password' => $request->password,
                're_password' => $request->re_password,
                'gender' => $request->gender,

            ), array(
                'firstName' => 'required ',
                'lastName' => 'required',
                'email' => 'required | email | unique:users',
                'password' => 'required',
                're_password' => 'required | same:password',
                'gender' => 'required',
            )
        );
        if ($validator->fails()) {
            return redirect('login')->withErrors($validator)->withInput();
        } else {

            //$res = $this->customer->signupProcess($request);
            //check email already exit
            // if ($res['email'] == "true") {
            //     return redirect('/login')->withInput($request->input())->with('error', Lang::get("website.Email already exist"));
            // } else {
            //     if ($res['insert'] == "true") {
            //         if ($res['auth'] == "true") {
            //             $result = $res['result'];
            //             Session::forget('guest_checkout');
            //             return redirect('/phone_verify');
            //         } else {
            //             return redirect('login')->with('loginError', Lang::get("website.Email or password is incorrect"));
            //         }
            //     } else {
            //         return redirect('/login')->with('error', Lang::get("website.something is wrong"));
            //     }
            // }

            $res = $this->customer->checkEmailExist($request);
            if($res){
                return redirect('login')->withInput($request->input())->with('error', Lang::get("website.Email already exist"));
            }else{
                session([
                    'new_user'=>array(
                        'firstname' => $request->firstName,
                        'lastname' => $request->lastName,
                        'email' => $request->email,
                        'gender' => $request->gender,
                        'password' => $request->password,
                    )
                ]);
                return redirect('/phone_verify');
            }
        }
    }

    public function verifyPhone(Request $request){
        $final_theme = $this->theme->theme();
        if(!session('new_user')) return redirect('/login');
        $title = array('pageTitle' => Lang::get("website.Phone Verification"));
        $result = array();
        $result['commonContent'] = $this->index->commonContent();
        return view("web.phone_verify", ['title' => $title, 'final_theme'=>$final_theme])->with('result', $result);
    }

    public function phoneVerifyProcess(Request $request){
        $data = $request->validate([
            'phone' => 'required|unique:users|numeric',
        ]);
        $twilio_sid = getenv("TWILIO_SID");
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        //echo $twilio_sid.'/'.$token.'/'.$twilio_verify_sid.'/'.$twilio_number;exit;
        try{
            $twilio = new Client($twilio_sid, $token);
            $twilio->verify->v2->services($twilio_verify_sid)
                ->verifications
                ->create('+1'.$data['phone'], "sms");
            return redirect()->route('phone_verify_confirm')->with(['phone' => $data['phone']]);
        }catch(\Exception $e){
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function confirmVerifyPhone(Request $request){
        $final_theme = $this->theme->theme();
        if(!session('new_user')) return redirect('/login');
        else if(!session('phone')) return redirect('/phone_verify');
        $title = array('pageTitle' => Lang::get("website.Confirm Phone Number"));
        $result = array();
        $result['commonContent'] = $this->index->commonContent();
        return view("web.phone_verify_confirm", ['title' => $title, 'final_theme'=>$final_theme])->with('result', $result);
    }

    public function phoneConfirmVerifyProcess(Request $request){
        $data = $request->validate([
            'verification_code' => ['required', 'numeric'],
            'phone' => ['required', 'string'],
        ]);
        /* Get credentials from .env */
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        try{
            $twilio = new Client($twilio_sid, $token);
            $verification = $twilio->verify->v2->services($twilio_verify_sid)
                ->verificationChecks
                ->create($data['verification_code'], array('to' => '+1'.$data['phone']));
            if ($verification->valid) {
                // $user = tap(User::where('phone', $data['phone']))->update(['phone_verified' => true]);
                $new_user = session('new_user');
                $new_user['phone'] = $request->phone;
                session(['new_user'=>$new_user]);
                return redirect('/photo_upload');
                /* Authenticate user */
                // Auth::login($user->first());
                // return redirect()->route('home')->with(['message' => 'Phone number verified']);
            }
            return back()->with(['phone' => $data['phone'], 'error' => 'Invalid verification code entered!']);
        }catch(\Exception $e){
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function photoUpload(Request $request){
        $final_theme = $this->theme->theme();
        if(!session('new_user')) return redirect('/login');
        $title = array('pageTitle' => Lang::get("website.Upload Your Photo"));
        $result = array();
        $result['commonContent'] = $this->index->commonContent();
        return view("web.photo_upload", ['title' => $title, 'final_theme'=>$final_theme])->with('result', $result);
    }

    public function photoUploadProcess(Request $request){
        if ($request->hasFile('file_id_card')) {
            //  Let's do everything here
            if ($request->file('file_id_card')->isValid()) {
                //
                $validated = $request->validate([
                    'file_id_card' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $extension = $request->file_id_card->getClientOriginalExtension();
                $imageName = time().'.'.$extension;
                $request->file_id_card->move(public_path('images/customers_id'), $imageName);
                //echo "success";exit;
                $new_user = session('new_user');
                $new_user['id_photo'] = $imageName;
                session(['new_user'=>$new_user]);
                //var_dump(session('new_user'));exit;
                
                $res = $this->customer->signupProcess($request);
                //check email already exit
                
                if ($res['insert'] == "true") {
                    if ($res['auth'] == "true") {
                        $result = $res['result'];
                        Session::forget('guest_checkout');
                        return redirect('/');
                    } else {
                        return redirect('login')->with('loginError', Lang::get("website.Email or password is incorrect"));
                    }
                } else {
                    return redirect('/login')->with('error', Lang::get("website.something is wrong"));
                }
            }
        }
        abort(500, 'Could not upload image :(');
    }
}
