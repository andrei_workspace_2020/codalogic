<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Core\Countries;
use App\Models\Core\Setting;
use App\Models\Core\Zones;
use App\Models\Core\Cities;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;

class CitiesController extends Controller
{
    //
    public function __construct(Cities $cities, Zones $zones, Countries $countries, Setting $setting)
    {
        $this->Countries = $countries;
        $this->Zones = $zones;
        $this->Cities = $cities;
        $this->Setting = $setting;
    }

    public function index(Request $request)
    {
        $title = array('pageTitle' => Lang::get("labels.ListingCities"));
        $cities = $this->Cities->paginator();
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.cities.index", $title)->with('result', $result)->with('cities', $cities);
    }

    public function add(Request $request)
    {
        $title = array('pageTitle' => Lang::get("labels.AddCity"));
        $result = array();
        $message = array();
        $countries = $this->Countries->getter();
        $result['countries'] = $countries;
        $result['message'] = $message;
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.cities.add", $title)->with('result', $result);
    }

    public function insert(Request $request)
    {
        $this->Cities->insert($request);
        $message = Lang::get("labels.CityAddedMessage");
        return Redirect::back()->with('message', $message);
    }

    public function edit(Request $request)
    {
        $title = array('pageTitle' => Lang::get("labels.EditCity"));
        $result = array();
        $result['message'] = array();

        $cities = $this->Cities->edit($request);
        $countries = $this->Countries->getter();
        $result['countries'] = $countries;
        $result['cities'] = $cities;
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.cities.edit", $title)->with('result', $result);
    }

    public function update(Request $request)
    {
        $this->Cities->updaterecord($request);
        $message = Lang::get("labels.City has been updated successfully");
        return Redirect::back()->with('message', $message);
    }

    public function delete(Request $request)
    {
        $this->Cities->deleterecord($request);
        return redirect()->back()->withErrors([Lang::get("labels.CityDeletedTax")]);
    }

    public function filter(Request $request)
    {
        $name = $request->FilterBy;
        $param = $request->parameter;
        $title = array('pageTitle' => Lang::get("labels.ListingZones"));
        $cities = $this->Cities->filter($request);
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.cities.index", $title)->with('result', $result)->with('cities', $cities)->with('name', $name)->with('param', $param);
    }

    public function getzonesfromcountry(Request $request){
        $countries_id = $request->countries_id;
        $zones = $this->Zones->getZonesFromCountry($request);
        return $zones;  
    }
}
