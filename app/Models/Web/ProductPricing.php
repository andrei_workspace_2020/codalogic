<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductPricing extends Model
{
    //
    public $timestamps = true;
    
    public function getpricings($request){
        $product_id = $request->products_id;
        $pricings = DB::table('product_pricings')->where('product_id', $product_id)->orderby('id', 'asc')->get();
        return $pricings;
    }
}