<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Support\Facades\DB;

class Cities extends Model
{
    //

    use Sortable;
    public function country(){
        return $this->belongsTo('App\Country');
    }

    public $sortable = ['city_id','zone_id','zone_name'];
    public $sortableAs = ['countries_name'];

    public function getter(){
        $cities = Cities::sortable(['city_id'=>'ASC'])
          ->LeftJoin('zones','cities.city_zone_id','=','zones.zone_id')
          ->LeftJoin('countries', 'zones.zone_country_id', '=', 'countries.countries_id')
        ->get();
      return $cities;
    }

    public function paginator(){
      $cities = Cities::sortable(['city_id'=>'ASC'])
        ->LeftJoin('zones','cities.city_zone_id','=','zones.zone_id')
        ->LeftJoin('countries', 'zones.zone_country_id', '=', 'countries.countries_id')
        ->paginate(30);
      return $cities;
    }

    public function filter($data){
        $name = $data['FilterBy'];
        $param = $data['parameter'];

        $result = array();
        $message = array();
        $errorMessage = array();

        switch ( $name ) {
            case 'City':
                $zones = Cities::sortable(['city_id'=>'ASC'])
                    ->LeftJoin('zones','zones.zone_id','=','cities.city_zone_id')
                    ->LeftJoin('countries','zones.zone_country_id','=','countries.countries_id')
                    ->where('cities.city_name', 'LIKE', '%' . $param . '%')
                    ->paginate(30);
                break;
            case 'Zone':
                $zones = Cities::sortable(['city_id'=>'ASC'])
                    ->LeftJoin('zones','zones.zone_id','=','cities.city_zone_id')
                    ->LeftJoin('countries','zones.zone_country_id','=','countries.countries_id')
                    ->where('zones.zone_name', 'LIKE', '%' . $param . '%')
                    ->paginate(30);
                break;
            case 'Country':
                $zones = Cities::sortable(['city_id'=>'ASC'])
                    ->LeftJoin('zones','zones.zone_id','=','cities.city_zone_id')
                    ->LeftJoin('countries','zones.zone_country_id','=','countries.countries_id')
                    ->where('countries.countries_name', 'LIKE', '%' . $param . '%')
                    ->paginate(30);
                break;
            default:
                $zones = Cities::sortable(['city_id'=>'ASC'])
                    ->LeftJoin('zones','zones.zone_id','=','cities.city_zone_id')
                    ->LeftJoin('countries','zones.zone_country_id','=','countries.countries_id')
                    ->paginate(30);
                break;
        }

        return $zones;
    }

    public function getcountries(){
        $countries = DB::table('countries')->get();
        return $countries;
    }


    public function insert($request){
        $city_id = DB::table('cities')->insertGetId([
            'city_zone_id'  	=>   $request->city_zone_id,
            'city_name'			=>   $request->city_name,
            'fee'               =>   $request->fee,
        ]);
        return $city_id;
    }

    public function edit($request){
        $cities =  DB::table('cities')
                    ->LeftJoin('zones', 'cities.city_zone_id', '=', 'zones.zone_id')
                    ->LeftJoin('countries', 'countries.countries_id', '=', 'zones.zone_country_id')
                    ->where('cities.city_id', $request->id)->first();
        return $cities;
    }


    public function updaterecord($request){
        DB::table('cities')->where('city_id', $request->city_id)->update([
            'city_name'  		 =>   $request->city_name,
            'city_zone_id'	 =>   $request->city_zone_id,
            'fee'               =>     $request->fee,
        ]);
    }
    public function getcountry($request){
        $country = DB::table('countries')->where('countries_id', $request->id)->get();
        return $country;
    }

    public function deleterecord($request){
      DB::table('cities')->where('city_id', $request->id)->delete();
    }



}
