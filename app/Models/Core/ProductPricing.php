<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductPricing extends Model
{
    //
    public $timestamps = true;
    
    public function getpricings($request){
        $product_id = $request->id;
        $pricings = DB::table('product_pricings')->where('product_id', $product_id)->orderby('id', 'asc')->get();
        return $pricings;
    }
    public function addpricings($request, $products_id){
        $costs = $request->costs;
        $units = $request->units;
        foreach($costs as $index=>$cost){
            $pricing = array(
                'product_id'=>$products_id,
                'cost'=>$cost,
                'unit'=>$units[$index]
            );
            DB::table('product_pricings')->insert($pricing);
        }
    }
    public function deletepricings($request){
        $product_id = $request->id;
        DB::table('product_pricings')->where('product_id', $product_id)->delete();
    }
}